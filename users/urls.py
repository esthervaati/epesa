from django.conf.urls import patterns, url
from .views import register_customer,login_user


urlpatterns = [
    url(r'^register_customer/$', register_customer),
    url(r'^login_user/$', login_user),
     
]