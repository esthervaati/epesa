from django.core.validators import validate_email
from django.contrib.auth.hashers import make_password
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser

from django.contrib.auth.base_user import BaseUserManager
from datetime import datetime

        
class User(AbstractUser):
    phone = models.CharField(max_length=50,blank=True,null=True)
    is_customer = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    physical_address = models.CharField(max_length=50,null=True)
    dob =models.DateTimeField(default =datetime.now()) 
    income = models.DecimalField(max_length=40,decimal_places=2,null=True)
    activation_status = models.BooleanField(default=False)
    loan_limit = models.DecimalField(max_digits=20, decimal_places=2,null=True)


    # def __unicode__(self):
    #     return str(self.phone)





