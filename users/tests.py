from django.test import TestCase, RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase
from django.test import Client
from rest_framework.test import APIClient
import unittest
import json


# Create your tests here.

class UsersTestCase(TestCase):
    """
    This class here tries to test the functionality of the user management module
    """
    def setUp(self):
        self.client = APIClient()
        self.Token = None
        User.objects.create(
              first_name="esther",
              last_name="vaati",
              email="vaatiesther@gmail.com",
              password ="nairobi",
              physical_address="nairobi",
              phone="0720000000",
              is_customer =True
          )
        
    def test_customer_can_be_added(self):
        """
        TEST: ADDING CUSTOMER USER
        ENDPOINT: /user/register_customer/
        METHOD: POST
        RESPONSE STATUS CODE: 201
        """
        user = {
            "first_name": "Esther",
            "last_name": "vaati",
            "password": "nairobi",
            "phone": "0725822798",
            "email": "essyking@gmail.com",
            "physical_address":"nairobi",
            "is_customer":True

              }
        response = self.client.post('/user/create/register_customer', user, format='json')
        assert response.status_code == 201, "did not add user"
        users = User.objects.all()
        assert users[0].first_name == "Esther", "User was not added correctly"

    def test_customer_can_be_added(self):
        """
        TEST: APPROVING CUSTOMER USER
        ENDPOINT: /user/approve_customer/
        METHOD: POST
        RESPONSE STATUS CODE: 201
        """
        user = ((User.objects.all())[0]).email
        response = self.client.post('/user/approve_customer', user, format='json')
        assert response.status_code == 201, "did not approve user"
       
        

  