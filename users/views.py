from .models import *
from  datetime import datetime
from django.contrib.auth import authenticate
from django.contrib.auth.models import Permission
from django.contrib.auth.hashers import make_password
from rest_framework.authtoken.models import Token
from django.contrib.auth.signals import user_logged_in
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import models
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework import status,permissions
from .serializers import UserSerializer
from .models import User

# Create your views here.

@api_view(['POST'])
def register_customer(request):
    """
    Endpoint: /user/register_user/
    Method: POST
    Allowed users: Register Users
    Response status code: 201 created
    Description: Creates users.
    """
    user_details = request.data
    user = User(
        first_name=user_details['first_name'],
        last_name=user_details['last_name'],
        phone=user_details['phone'],
        email=user_details['email'],
        password=make_password(user_details['password']),
        physical_address= user_details['physical_address'],
        is_customer=True

        )
    user.save()
    return Response({"success":"users created successfully"},status=status.HTTP_CREATED_201)
    
@api_view(['POST'])
def login_user(request):
    """
    Endpoint: /user/userlogin/
    Method: POST
    Allowed users: All users
    Response status code: 200 success
    Description: Logs in a user
    """
    try:
        email = request.data['username']
        password = request.data['password']
        
        user = authenticate(email=email, password=password)
        
        if user and user.is_active:
            login_token=Token.objects.get_or_create(user=user)
            user_details = {}
            user_details['user'] = "%s %s" % (user.first_name, user.last_name)
            if user.is_customer:
                user_details['role'] = 'customer'
               
            elif user.is_admin:
                user_details['role'] = 'admin'
            user_details['token'] = login_token
            user_logged_in.send(sender=user.__class__, request=request, user=user)
            return Response(user_details,status=status.HTTP_200_OK)
        else:
            res = {'error': 'can not authenticate with the given credentials'}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)
    except KeyError:
        res = {'error': 'please provide a username and a password'}
        return Response(res, status=status.HTTP_400_BAD_REQUEST)
        
@api_view(['PUT'])
def approve_customer(request):
    """
    Endpoint: /loan/approve_customer/
    Method: PUT
    Allowed users: Admin
    Response status code: 204 no content
    Description: Used to approve a customer's account before they can access loans
    """
    if not check_permission(request.user, 'can_approve_customers'):
        return Response({'error': "can not approve customers"}, status=status.HTTP_403_FORBIDDEN)
    email = request.data['email']
    try:
        user = User.objects.get(email=email)
        user.activation_status = True
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except DoesNotExist:
        return Response({'error': "not found"}, status=status.HTTP_404_NOT_FOUND)



     



