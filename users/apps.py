from django.apps import AppConfig

class UsersAppconfig(AppConfig):
	name = 'users'

	def ready(self):
		import users.models
		import users.signals