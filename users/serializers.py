from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from .models import *



class PermissionSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    """This is intended to power a read-only view"""
    class Meta:
        model = Permission
        fields = ('id', 'name', 'codename',)

class GroupSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    name = serializers.CharField(validators=[])
    permissions = PermissionSerializer(many=True,read_only=True, required=False)

    class Meta:
        model = Group
        fields = ('id','name','permissions')

class UserSerializer(serializers.ModelSerializer):

    id = serializers.ReadOnlyField()
    user_permissions = PermissionSerializer(many=True,read_only=True,
                                           required=False)

    class Meta(object):
        model = User
        fields = ('id','first_name','last_name', 'email','password','phone',
                  'is_customer','is_admin','physical_address','activation_status'
                  'user_permissions')
        



