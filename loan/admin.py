from django.contrib import admin
from .models import Loan,LoanBalance,Disbursement

# Register your models here.
admin.site.register(Loan)
admin.site.register(LoanBalance)
admin.site.register(Disbursement)
