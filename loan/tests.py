from django.test import TestCase, RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase
from django.test import Client
from rest_framework.test import APIClient
import unittest
import json

from .models import Loan,LoanBalance

# Create your tests here.

class UsersTestCase(TestCase):
    """
    This class here tries to test the functionality of the user management module
    """
    def setUp(self):
        self.client = APIClient()
        self.Token = None
        User.objects.create(
              first_name="esther",
              last_name="vaati",
              email="vaatiesther@gmail.com",
              password ="nairobi",
              physical_address="nairobi",
              phone="0720000000",
              is_customer =True
          )
        user = (User.objects.all())[0]
        Loan.objects.create(
        	customer = user,
			transaction_id = "qwerty",
			date = "2017-12-12",
			amount_borrowed = "1000",
			start_date = "2017-12-12",
			end_date ="2017-12-12",
			period = "weekly",
			interest = 75,
			principal_amount = 1075,
			channel = "mpesa"
        	)
        loanid = (Loan.objects.all())[0]

        Disbursement.objects.create(
        	customer = user,
			transaction_id = "qwerty",
			loan_id = loanid,
			date = "2017-12-12"
        	)
        LoanBalance.objects.create(
        	customer = user,
			loan_id = loanid,
			amount_to_pay = 1075
        	)

        
    def test_customer_can_request_loan(self):
        """
        TEST: REQUEST LOAN
        ENDPOINT: /loan/request_loan/
        METHOD: POST
        RESPONSE STATUS CODE: 201
        """
        customer= request.user,
        transaction_id=create_hash(),
        amount_borrowed=loan_details['amount_borrowed'],
        channel = loan_details['channel'],
        period = loan_details['period'],
        status = is_active
        loan= {
            "customer": user,
            "transaction_id":loan_id,
            "amount_borrowed": "1075",
            "channel": "mpesa",
            "period": "WEEKLY",
              }
        response = self.client.post('/loan/create/request_loan', loan, format='json')
        assert response.status_code == 201, "loan unsuccessful"
        

    def test_remainder_messages_can_be_sent(self):
        """
        TEST: SENDING REMAINDER MESSAGES
        ENDPOINT: /loan/send_remainder_messages/
        METHOD: POST
        RESPONSE STATUS CODE: 201
        """
        phone = (User.objects.all()[0]).phone
 
        response = self.client.post('/loan/send_remainder_messages',phone  format='json')
        assert response.status_code == 201, "remainders not sent"
       
        

  