__all__ = ['User']
from users.models import *
from django.db import models
from datetime import datetime, timedelta
from decimal import Decimal
from django.utils import timezone
import uuid


channel_types = (
    ("MPESA", "MPESA"),
    ("AIRTELMONEY", "AIRTELMONEY"),
)

period_types = (
    ("WEEKLY", "WEEKLY"),
    ("MONTHLY", "MONTHLY"),
    ("YEARLY", "YEARLY"),
)

loan_status = (
    ("ACTIVE", "ACTIVE"),
    ("INACTIVE", "INACTIVE"),
)

class Loan(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    customer = models.ForeignKey(User,null=True)
    transaction_id = models.CharField(max_length=50,unique=True)
    date =models.DateTimeField(default =datetime.now())
    amount_borrowed = models.DecimalField(max_digits=20, decimal_places=2,null=True)
    start_date =models.DateTimeField(default =datetime.now()) 
    end_date = models.DateTimeField(null=True)
    period = models.CharField(max_length=20,choices =period_types)
    interest =models.DecimalField(max_digits=20, decimal_places=2,null=True)
    principal_amount= models.DecimalField(max_digits=20, decimal_places=2,null=True)
    status=models.CharField(max_length=50,choices=loan_status,null=True)

    def save(self,*args,**kwargs):
        
        if self.period == "YEARLY":
            self.principal_amount = self.amount_borrowed + Decimal.from_float(0.16) 
            self.end_date = self.start_date + timedelta(days=365)
        elif self.period == "MONTHLY":
            self.principal_amount = self.amount_borrowed + Decimal.from_float(0.16) 
            self.end_date = self.start_date+ timedelta(days=30)
        elif self.period == "WEEKLY":
            self.principal_amount = self.amount_borrowed + Decimal.from_float(0.16) 
            self.end_date = self.start_date + timedelta(days=7)

        super(Loan,self).save(*args,**kwargs)

    def __unicode__(self):
        return self.transaction_id

class Disbursement(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    loan_id =models.ForeignKey(Loan,null=True)
    customer =models.ForeignKey(User,null=True)
    channel =models.CharField(max_length=50,choices=channel_types,null=True )
    transaction_id =models.CharField(max_length=50,unique=True,null=True)
    date =models.DateTimeField(null=True)

    def save(self,*args,**kwargs):
        super(Disbursement,self).save(*args,**kwargs)

    def __unicode__(self):
        return self.transaction_id


class LoanBalance(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    loan_id = models.ForeignKey(Loan,null=True)
    customer = models.ForeignKey(User,null=True)
    amount_to_pay = models.DecimalField(max_digits=20, decimal_places=2,null=True)

    def save(self,*args,**kwargs):
        super(LoanBalance,self).save(*args,**kwargs)

    def __unicode__(self):
        return str(self.amount_to_pay)

class Remainder(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    customer = models.ForeignKey(User,null=True)
    loan = models.ForeignKey(Loan,null=True)
    date = models.CharField(max_length=50,null=True)

    def save(self,*args,**kwargs):
        super(Remainder,self).save(*args,**kwargs)

    def __unicode__(self):
        return self.loan_balance

class Logs(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    description = models.CharField(max_length=100,null=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def save(self,*args,**kwargs):
        super(Logs,self).save(*args,**kwargs)

    def __unicode__(self):
        return self.description

class LoanScore(models.Model):
    customer = models.ForeignKey(User,null=True)
    income = models.DecimalField(max_digits=20, decimal_places=2,null=True)
    age =models.CharField(max_length=20,null=True)

    def save(self,*args,**kwargs):
        super(LoanScore,self).save(*args,**kwargs)

    def __unicode__(self):
        return self.income

class LoanPermissions(models.Model):
    """
    This model lists all the permissions for the loans module
    """
    class Meta:
        managed = False 
        permissions = (
            
            ('can_approve_loans', 'can approve loans'),
            ('can_reject_loans', 'can reject loans'),    
        )