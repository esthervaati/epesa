from rest_framework import serializers
from .models import Loan,LoanBalance,Payment,Remainder,Disbursement,Logs

class LoanSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = Loan
        fields = ('id', 'customer', 'transaction_id','date','amount_borrowed',
        	'start_date','end_date','interest','principal_amount','status')

class LoanSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = Disbursement
        fields = ('id', 'loan_id', 'customer','date','channel',
        	'transaction_id','date')

class LoanBalancetSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = LoanBalance
        fields = ('id', 'loan_id', 'loan_amount')


class RemainderSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = Remainder
        fields = ('id', 'customer', 'loan','date')

class LogsSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = Logs
        fields = ('id', 'description')




 