from django.apps import AppConfig


class LoanAppconfig(AppConfig):
    name = 'loan'

    def ready(self):
		import loan.models
		import loan.signals

