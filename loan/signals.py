from django.dispatch import receiver
from rest_framework.response import Response
from django.db.models.signals import post_save
from .models import Loan,Disbursement,LoanBalance,Logs
from .tasks import send_to_airtel,send_to_mpesa


@receiver(post_save, sender=Loan,dispatch_uid='save_loan_disbursement_details')
def save_loan_disbursement_details(sender, instance, **kwargs):
	print instance
	print "s"
	loan_details ={}
	loan_details['loan_id'] = instance
	loan_details['customer']=instance.customer
	loan_details['transaction_id']=instance.transaction_id
	loan_details['channel']=instance.channel
	disbursement  = Disbursement(**loan_details)
	disbursement.save()

	repayment = {}
	repayment['loan_id'] = instance
	repayment['amount_to_pay'] = instance.principal_amount
	loan_to_pay = LoanBalance(**repayment)
	loan_to_pay.save()

	if instance.channel == "MPESA":
		send_to_mpesa()
	elif instance.channel == "AIRTELMONEY":
		send_to_airtel()


@receiver(post_save, sender=Disbursement,dispatch_uid='save_loan_logs')
def save_logs(sender, instance, **kwargs):
	MESSAGE = """ %s, has received a loan of %s on %s"""
	customer = instance.customer.first_name
	loan_amount = instance.loan_id.principal_amount

	description = MESSAGE % (customer,loan_amount,instance.date) 
    
	log_details = {}
	log_details['description'] = description
	log_details['timestamp'] = instance.date
	log = Logs(**log_details)
	log.save()











	

    