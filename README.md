### Epesa is a loan management system that enables customers to register, access loans and the loan is sent to the channel of their preference, i.e MPESA or AIRTELMONEY 

1. Core Tools:
* Python
* Django Framework - Underlyging framework
* Pip - Python's package manager
* Postgres - Database

2.Project Setup
## The following libraries/applications need to have been installed on your machine: ##
* Django-celery
* Postgres
* Python Virtual Enviroment
* Python
3. Initial Setup:
* Activate your virtual enviroment 
* Run: pip install -r requirementst.txt ( to install the project dependencies)
*create database and run migrations)